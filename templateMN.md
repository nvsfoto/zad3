---
author: Karol Wozniak
title: Diablo II - Ważne słowa runiczne
subtitle: Słowa runiczne	
date: 08-11-2020
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
	\usepackage{graphicx}
	\graphicspath{ {./pics/} }
	\usepackage{epstopdf}
---



## Diablo II

\begin{center} \includegraphics[width=4cm]{d2.jpg} \end{center}

**Diablo 2** należy do gier typu ***RPG***. Można w niej znaleść cztery aktów. Z kolei w każdym z nich jest sześć zadań. Mamy możliwość przemierzania tych krain jedną z pięciu klas postaci \textcolor{green}{Amazonka}, \textcolor{blue}{Czarodziejka}, \textcolor{red}{Barbarzyńca}, \textcolor{gray}{Paladyn}, \textcolor{blue}{Nekromanta}.

\begin{center} Wyjątkowo w czwartym akcie są tylko trzy zadania \end{center}


## Kamienie runiczne pospolite

Bardzo ważne w grze diablo są ulepszenia przedmiotów przez słowa runiczne składane z kamieni runicznych.
Wyróżniamy zwroty runiczne *pospolite* w grze Diablo II:
<!--- UWAGA WAŻNE pusta linia odstępu -->

1. \includegraphics[width=0.5cm]{r-el.jpg} El
2. \includegraphics[width=0.5cm]{r-eld.jpg} Eld
3. \includegraphics[width=0.5cm]{r-eth.jpg} Eth
4. \includegraphics[width=0.5cm]{r-Ith.jpg} Ith
5. \includegraphics[width=0.5cm]{r-nef.jpg} Nef
6. \includegraphics[width=0.5cm]{r-tir.jpg} Tir

## Kamienie runiczne unikatowe

W grze można dodatkowo znaleźć również Kamienie tzw. unikatowe. Ich szansa na wypadnięcie w czasie losowania nagrody wynosi mniej niż 0,16 %:

::: {.columns}
:::: {.column width=0.25}
- \includegraphics[width=0.25cm]{r-tal.jpg}Tal
- \includegraphics[width=0.25cm]{r-ral.jpg}Ral
- \includegraphics[width=0.25cm]{r-ort.jpg}Ort
- \includegraphics[width=0.25cm]{r-thul.jpg}Thul
- \includegraphics[width=0.25cm]{r-amn.jpg}Amn
- \includegraphics[width=0.25cm]{r-sol.jpg}Sol
::::
:::: {.column width=0.25}
- \includegraphics[width=0.25cm]{r-pul.jpg}Pul
- \includegraphics[width=0.25cm]{r-um.jpg}Um
- \includegraphics[width=0.25cm]{r-mal.jpg}Mal
- \includegraphics[width=0.25cm]{r-ist.jpg}Ist
- \includegraphics[width=0.25cm]{r-gul.jpg}Gul
- \includegraphics[width=0.25cm]{r-vex.jpg}Vex
::::
:::: {.column width=0.25}
- \includegraphics[width=0.25cm]{r-shae.jpg}Shael
- \includegraphics[width=0.25cm]{r-dol.jpg}Dol
- \includegraphics[width=0.25cm]{r-hel.jpg}Hel
- \includegraphics[width=0.25cm]{r-io.jpg}Io
- \includegraphics[width=0.25cm]{r-lum.jpg}Lum
- \includegraphics[width=0.25cm]{r-ko.jpg}Ko
- \includegraphics[width=0.25cm]{r-fal.jpg}Fal
- \includegraphics[width=0.25cm]{r-lem.jpg}Lem
::::
:::: {.column width=0.25}
- \includegraphics[width=0.25cm]{r-ohm.jpg}Ohm
- \includegraphics[width=0.25cm]{r-lo.jpg}Lo
- \includegraphics[width=0.25cm]{r-sur.jpg}Sur
- \includegraphics[width=0.25cm]{r-ber.jpg}Ber
- \includegraphics[width=0.25cm]{r-jo.jpg}Jah
- \includegraphics[width=0.25cm]{r-cham.jpg}Cham
- \includegraphics[width=0.25cm]{r-zod.jpg}Zod
::::
:::

## Słowa runiczne

Ważnym elementem kamieni runicznych jest możliwość łączenia ich w słowa, pozwalając zwielokrotnić premie przyznawane elementom wyposażenia.

Przykładowe słowa runiczne składające się z kamieni pospolitych i  kamieni unikalnych:

::: {.columns}
:::: {.column width=0.5}
* \includegraphics[width=0.25cm]{r-dol.jpg}Dol
  * \includegraphics[width=0.25cm]{r-io.jpg}Io
::::
:::: {.column width=0.5}
* \includegraphics[width=0.5cm]{r-nef.jpg} Nef 
  * \includegraphics[width=0.25cm]{r-sol.jpg}Sol
    * \includegraphics[width=0.5cm]{r-Ith.jpg}Ith
::::
:::

## Słowa Runiczne - uwagi

\begin{alertblock}{Uwaga dla DOL + IO:}
Słowo runiczne można ułożyć tylko w przedmiocie typu różdżka
\end{alertblock}

\begin{alertblock}{Uwaga dla NEF + SOL + ITH}
Słowo runiczne można ułożyć tylko w przedmiotach typu:

Hełm

Cyrkletka

Hełm Barbarzyńcy

Hełm Druida (Dodatek *"Lord Of Destruction"*)

\end{alertblock}

<!--- Niestety pomiędzy tak zdefiniowanymi  blokami nie można umieszczać innych elementów-->

## Słowa Runiczne - premia DOL + IO

\begin{block}{Właściwości DOl + IO}

Trafienie zmusza potwora do ucieczki: 25% szans

+10 do żywotności

+3 do Umiejetnosci Trucizn oraz Kosci (tylko dla Nekromanty)

+3 do Zbroi z Kosci [tylko dla Nekromanty]

+2 do Wlóczni z Kosci [tylko dla Nekromanty]

+4 do Mistrzostwa we Wladaniu Szkieletami [tylko dla Nekromanty]

Obrazenia od magii zmniejszone o 4

20\% szybsze rzucanie czarów

+13 do many

\end{block}

## Słowo runiczne - Druid

\begin{center}

\includegraphics{bestia.png}

\bf Bestia

\bf minimalny poziom postaci:  \textcolor{red}{\bf 63}

\end{center}

| Przedmioty    | Właściwości	|
| ------------- |:-------------|
| Topór      | Poziom 9 Aura Fanatyzm |
| Młot      | +40% zwiekszona szybkosc ataku      |
| Berło | +240-270% zwiekszone obrazenia      |
|  | 20% szansa na Druzgocace Uderzenie      |
|  | 25% szansa Otwarcia Rany      |